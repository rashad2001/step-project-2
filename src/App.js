import React, {Component} from 'react';
import Header from "./Components/Header/Header";
import Home from "./Components/Home/Home";
import ModalWindow from "./Components/ModalWindow/ModalWindow";
import {Route} from "react-router";
import {BrowserRouter} from "react-router-dom";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {

            albums:[]
        }
    }
    componentDidMount() {
        fetch('../../../db.json')
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    albums: data
                });
            })
    }

    render() {
        return (

            <div className='App'>
                {


                 this.state.albums.length > 0 ? <Home albums={this.state.albums}/> : null

                }
            </div>



        );
    }
}

export default App;