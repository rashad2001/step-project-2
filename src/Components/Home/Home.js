import React, {Component} from 'react';
import Router from "../Router/Router";
import NoteItem from "../NoteItem/NoteItem";
import Header from "../Header/Header";
import Notes from "../Notes";
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCards: props.albums
        }
    }
    render() {
        return (
            <div>
                <Header/>
                <NoteItem albums={this.state.allCards} />
                <Router/>

            </div>
        );
    }
}

export default Home;