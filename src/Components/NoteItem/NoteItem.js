import React, {Component} from 'react';
import './NoteItem.scss';
class NoteItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            albums: props.albums,
            starColor:true,
            id:props.id,
            onclick: props.onclick
        }

    }

    render() {
        const albums = this.state.albums;
        return (
            <div className='album'>
                {
                    albums.map((card, ind) => {
                        return  <div key={ind} className="card">
                            <div className='list-info'>
                                <h6 className='list-name'>{card.name}</h6>
                                <p className='list-text' >{card.text}</p>




                            </div>
                        </div>
                    })
                }

            </div>
        );
    }
}

export default NoteItem;