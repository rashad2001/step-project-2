import React, {Component} from 'react';
import {Route} from "react-router";
import {BrowserRouter} from "react-router-dom";
import CreatePage from "../CreatePage/CreatePage";
import Home from "../Home/Home";
class Router extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>

                    <Route path={'/create'}>
                        < CreatePage
                        />
                    </Route>
                </BrowserRouter>

            </div>
        );
    }
}

export default Router;