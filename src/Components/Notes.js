import React, {Component} from 'react';
import NoteItem from "./NoteItem/NoteItem";

class Notes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCards: props.albums
        }
    }
    render() {
        return (
            <div>
                <NoteItem albums={this.state.allCards} />

            </div>
        );
    }
}

export default Notes;