import React, {Component} from 'react';
import "./CreatePage.scss"
class CreatePage extends Component {
    state = {
        title: "",
        text: '',
        id: JSON.stringify(Math.floor(Math.random() * Math.floor(10000))),
        color: '',
        isColorChosen: false,
        isCompletedBool: false
    };

    handleNote = (e) => {
        e.preventDefault();
        this.setState({isCompletedBool: true});
    };

    async createNote(e) {
        e.preventDefault();
        let result = await fetch("http://localhost:3000/notes", {
            method: 'post',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                id: this.state.id,
                title: this.state.title,
                text: this.state.text,
                color: this.state.isColorChosen ? this.state.color : this.generateColor(),
                isCompleted: this.state.isCompletedBool ? "true" : "false"
            })

        });
        this.setState({title: ""});
        this.setState({text: ""});

    };

    render() {
        return (
            <div>
                <form className="form-class">
                    <h1>Fill data</h1>
                    <input type="text" className="title-input" placeholder="title"/>
                    <input type="text" className="text-input" placeholder="Note text"/>
                    <button type="submit" className="button-submit">Create</button>
                </form>
            </div>
        );
    }
}

export default CreatePage;