import React, {Component} from 'react';
import './Header.scss';
import {Link} from "react-router-dom";
import {BrowserRouter} from "react-router-dom";

class Header extends Component {


    render() {
        return (
            <div className="app">
               <BrowserRouter>
                <span className="header-link"> <Link to={'/'}>Notes App</Link> </span>
                <Link to={'/actual'}><button className="header-button">Actual</button></Link>
              <Link to={'/create'}><button className="header-button">Create</button></Link>
            </BrowserRouter>
    </div>
        )
    }
}
export default Header;